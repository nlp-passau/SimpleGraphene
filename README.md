# DEPRECATED
This repository is deprecated and will no longer be updated, maintained or watched.

**All future development is done at [https://github.com/Lambda-3/Graphene](https://github.com/Lambda-3/Graphene).**



# ~~Simple Relation Extraction~~
_Sentence Simplification and Open Relation Extraction_

This tool and library wraps the two projects "sentence simplifcation"
and "OpenIE" in one project to simply extract relations from sentences.
 
## Building
It is better to have a maven nexus account within NLP-Group of the University of Passau.
If not install the following packages:
    
    git clone --branch v3.0.1-SNAPSHOT git@gitlab.com:ie-pipeline/sentence_simplification.git
    cd sentence_simplification
    mvn install
    
    git clone --branch v1.1.1 git@gitlab.com:ie-pipeline/discourse_simplification.git
    cd discourse_simplification
    mvn install
    
Then, use maven to create the jar file.

    mvn clean package

## Running
    java -jar target/SimpleGraphene-jar-with-dependencies.jar -s "In 2016, this tool was developed."
Possible options:

* -s: one single sentence to work on
* -i: input file with line separated sentences
* -t: type of running
    * SIMPLIFY: do sentence simplification
    * OPENIE: do relation extraction
    * BOTH: do both (default)
* -o: output file
* -c: write output to console (default)

The output is in JSON format using Google's GSON library.

## Using as library
    List<String> sentences = new ArrayList<>();
    sentences.add("The U.S. president Barack Obama gave his speech on Tuesday to thousands of people.");
    
    // simplified
    List<SimplifiedSentence> simplified = SimpleRelationExtraction.simplify(sentences);
    
    // extractions
    List<OpenIEExtraction> extractions = SimpleRelationExtraction.extract(sentences);
    
    // simplification and extraction
    List<SimplifiedOpenIEExtraction> simplifiedAndExtracted = SimpleRelationExtraction.simplifyThenExtract(sentences);
